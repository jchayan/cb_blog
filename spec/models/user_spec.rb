require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) {
      User.create!(:name => 'John Doe', :email => 'john@doe.com')
  }

  context "with no posts" do
    it "should not contain any posts" do
      expect(user.reload.posts).to eq([])
    end
  end

  context "with posts" do
    it "should order posts chronologically" do
      post1 = user.posts.create!(:title => 'Title 1', :content => 'Content 1')
      post2 = user.posts.create!(:title => 'Title 2', :content => 'Content 2')
      expect(user.reload.posts).to eq([post2, post1])
    end
  end
end
