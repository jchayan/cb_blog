class PostsController < ApplicationController
  before_action :set_author, only: [:index, :new, :show, :create, :edit, :update, :destroy]
  before_action :set_post, only: [:show, :read, :edit, :update, :destroy]

  # Views API
  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
  end

  def edit
  end

  # CRUD API
  def create
    @post = Post.new(post_params)
    @post.author = @author

    if @post.save
      render json: {
        :message => 'Post succesfully created',
        :post_id => @post.id
      }, status: :ok
    else
      render json: {
        :message => 'Failed to create new post',
        :details => @post.errors
      }, status: :unprocessable_entity
    end
  end

  def read
    render json: @post
  end

  def update
    if @post.update(post_params)
      render json: {
        :message => 'Post was successfully updated.'
      }, :status => :ok
    else
      render json: {
        :message => "Failed to update post (ID: #{params[:id]})",
        :details => @post.errors
      }, status: :unprocessable_entity
    end
  end

  def destroy
    if @post.present?
      @post.destroy
      head :no_content
    else
      render json: {
        :message => 'Post does not longer exists',
        :details => nil
      }, status: :not_found
    end
  end

  private
    def set_author
      @author = User.find(params[:user_id])
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :content, :author_id)
    end
end
