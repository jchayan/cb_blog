class UsersController < ApplicationController
  before_action :set_user, only: [:show, :read, :edit, :update, :destroy]
  before_action :authorize, only: [:auth]

  # Views API
  def index
    @users = User.all
    render json: @users
  end

  def new
    @user = User.new
  end

  def show
    @posts = @user.posts
  end

  def edit
  end

  def login
  end

  # CRUD API
  def create
    name         = user_params[:name]
    email        = user_params[:email]
    avatar_image = user_params[:avatar_image]

    if avatar_image.present?
      extension = File.extname avatar_image.original_filename
      file_url  = "uploads/#{SecureRandom.uuid}#{extension}"
      file_path = Rails.root.join 'public', file_url

      File.open(file_path, 'wb') do |file|
        file.write(avatar_image.read)
      end

      avatar_image = file_url
    end

    @user = User.new(name: name, email: email, avatar_image: avatar_image)

    if @user.save
      render json: {
        :message => 'User succesfully created',
        :user_id => @user.id
      }, status: :ok
    else
      render json: {
        :message => 'Failed to create new user',
        :details => @user.errors
      }, status: :unprocessable_entity
    end
  end

  def read
    if @user.present?
      render json: @user
    else
      render json: {
        :message => 'User not found',
        :details => ''
      }, status: :not_found
    end
  end

  def update
    name         = user_params[:name]
    email        = user_params[:email]
    avatar_image = user_params[:avatar_image]

    if avatar_image.present?
      extension = File.extname avatar_image.original_filename
      file_url  = "uploads/#{SecureRandom.uuid}#{extension}"
      file_path = Rails.root.join('public', file_url)

      File.open(file_path, 'wb') do |file|
        file.write(avatar_image.read)
      end

      avatar_image = file_url

      # Delete previous avatar from disk
      if @user.avatar_image.present?
        old_avatar_path = Rails.root.join 'public', @user.avatar_image

        if File.exists? old_avatar_path
          File.delete old_avatar_path
        end
      end
    end

    if @user.update(name: name, email: email, avatar_image: avatar_image)
      render json: {
        :message => 'User was successfully updated.'
      }, :status => :ok
    else
      render json: {
        :message => "Failed to update user (ID: #{params[:id]})",
        :details => @user.errors
      }, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.present?
      @user.destroy
      head :no_content
    else
      render json: {
        :message => 'User does not longer exists',
        :details => nil
      }, status: :not_found
    end
  end

  # Just check if the user exists
  def auth
    if @user.present?
      redirect_to :action => 'show', :id => @user.id
    else
      redirect_to root_path, flash: { :error => "User not found" }
    end
  end

  private
    # Dummy authorization middleware
    def authorize
      @user = User.find_by_email(user_params[:email])
    end

    def set_user
      @user = User.find_by_id(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :avatar_image)
    end
end
