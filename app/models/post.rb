class Post < ApplicationRecord
  validates :title, presence: true
  validates :content, presence: true
  default_scope { order(published_at: :desc) }

  before_create do
    self.published_at = DateTime.now
  end

  belongs_to :author, :class_name => :User, :foreign_key => :author_id
end
