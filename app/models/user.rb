class User < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true

  has_many :posts, :foreign_key => 'author_id'
end
