Rails.application.routes.draw do
  resources :users
  root "users#login"
  resources :users do
    collection do
      post :auth
    end

    member do
      get  :read
    end

    resources :posts
  end
end
